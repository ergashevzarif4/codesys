package com.example.zarif.codesys.listeners

import com.example.zarif.codesys.moduls.Verifiction
import com.example.zarif.codesys.moduls.VerifictionAdd
import com.example.zarif.codesys.moduls.VerifictionForAdd

interface ICheckTimeTableHelper {
    fun addRequestVerification(verifiction: VerifictionForAdd)
    fun erorr(message: String)
}