package com.example.zarif.codesys.db.group;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class GroupEntity {
    @PrimaryKey(autoGenerate = true)
    public Long group_entity_id;
    public String groupId;
    public String groupName;
    public String groupKafedraName;
    public String groupTimeTableID;
    public String students;

}
