package com.example.zarif.codesys.moduls.reponceModuls;

public class LoginResponce {
    private Boolean isStudent;
    private String userId;
    private String message;

    public LoginResponce() {
    }

    public LoginResponce(Boolean isStudent, String userId, String message) {
        this.isStudent = isStudent;
        this.userId = userId;
        this.message = message;
    }

    public Boolean getIsStudent() {
        return isStudent;
    }

    public void setIsStudent(Boolean student) {
        isStudent = student;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "LoginResponce{" +
                "isStudent=" + isStudent +
                ", userId='" + userId + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
