package com.example.zarif.codesys.network.data;

import java.util.ArrayList;
import java.util.List;

public class TimeTable {
    private String timeTableId;
    private List<SubTimeTable> subTimeTables;

    protected TimeTable() {
        subTimeTables = new ArrayList<>();
    }

    public TimeTable(List<SubTimeTable> subTimeTables) {
        this.subTimeTables = subTimeTables;
    }

    public String getTimeTableId() {
        return timeTableId;
    }


    public List<SubTimeTable> getSubTimeTables() {
        return subTimeTables;
    }
}
