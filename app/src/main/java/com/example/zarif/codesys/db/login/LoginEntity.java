package com.example.zarif.codesys.db.login;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class LoginEntity {
    @PrimaryKey(autoGenerate = true)
    public Long login_entity_id;
    public String userId;
    public Boolean isStudent;

}
