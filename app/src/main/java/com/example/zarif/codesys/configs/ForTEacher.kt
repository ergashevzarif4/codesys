package com.example.zarif.codesys.configs

import com.example.zarif.codesys.db.DBManager
import com.example.zarif.codesys.enum.*
import com.example.zarif.codesys.listeners.ITeacherTimeTableHelper
import com.example.zarif.codesys.network.data.SubTimeTable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*

//todo qr code generatsiya qilishdan oldin tekshirish uchun
class ForTEacher(private val dbManager: DBManager) {

    fun checkTeacher(iTeacherTimeTableHelper: ITeacherTimeTableHelper) {
        val subtimeTableString = dbManager.timeTableDao().loadTimeTable().subTimeTables

        val typ: Type = object : TypeToken<List<SubTimeTable>>() {}.type
        var subTimeTable: MutableList<SubTimeTable> = Gson().fromJson(subtimeTableString, typ)
        var teacherSubTimeTable: SubTimeTable? = null
        for (subTimeTable in subTimeTable) {
            if (checkTime(subTimeTable.time, subTimeTable.dayOfWeek)) {
                teacherSubTimeTable = subTimeTable
                break
            }
        }
        if (teacherSubTimeTable == null) {
            iTeacherTimeTableHelper.erorr("lesson no found")
        } else {
            iTeacherTimeTableHelper.generateBarCode(teacherSubTimeTable)
        }
    }

    private fun checkTime(time: String?, dayOfWeek: String?): Boolean {
//todo dayofWekni tekshiramiz
        val b: Boolean
        val a: Boolean
        val date = Calendar.getInstance()
        if (time == BIRINCHI_PARA && date.get(Calendar.HOUR_OF_DAY) == 8 && date.get(Calendar.MINUTE) < 35 && date.get(Calendar.MINUTE) > 20)
            b = true
        else if (time == IKKINCHI_PARA && (date.get(Calendar.HOUR_OF_DAY) == 9 && (date.get(Calendar.MINUTE) > 50) || (date.get(Calendar.HOUR_OF_DAY) == 10 && date.get(Calendar.MINUTE) < 5)))
            b = true
        else if (time == UCHUNCHI_PARA && date.get(Calendar.HOUR_OF_DAY) == 11 && date.get(Calendar.MINUTE) < 35 && date.get(Calendar.MINUTE) > 20)
            b = true
        else if (time == TURTINCHI_PARA && date.get(Calendar.HOUR_OF_DAY) == 13 && date.get(Calendar.MINUTE) < 35 && date.get(Calendar.MINUTE) > 20)
            b = true
        else if (time == BESHINCHI_PARA && (date.get(Calendar.HOUR_OF_DAY) == 14 && (date.get(Calendar.MINUTE) > 50) || (date.get(Calendar.HOUR_OF_DAY) == 15 && date.get(Calendar.MINUTE) < 5)))
            b = true
        else b = time == OLTINCHI_PARA && date.get(Calendar.HOUR_OF_DAY) == 16 && date.get(Calendar.MINUTE) < 35 && date.get(Calendar.MINUTE) > 20

        val dayofWeek = date.get(Calendar.DAY_OF_WEEK)
        if (dayofWeek == 1)
            a = dayOfWeek == "sun"
        else if (dayofWeek == 2)
            a = dayOfWeek == "mon"
        else if (dayofWeek == 3)
            a = dayOfWeek == "tue"
        else if (dayofWeek == 4)
            a = dayOfWeek == "wed"
        else if (dayofWeek == 5)
            a = dayOfWeek == "thu"
        else if (dayofWeek == 6)
            a = dayOfWeek == "fri"
        else if (dayofWeek == 7)
            a = dayOfWeek == "sut"
        else a = false

        return a && b
    }
}