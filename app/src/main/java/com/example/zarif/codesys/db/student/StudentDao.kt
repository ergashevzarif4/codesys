package com.example.zarif.codesys.db.student

import android.arch.persistence.room.*
import com.example.zarif.codesys.moduls.Student

@Dao()
interface StudentDao {
    @Insert
    fun addStudent(student: StudentEntitiy)

    @Delete
    fun deleteStudent(student: StudentEntitiy)

    @Update
    fun updateStudent(student: StudentEntitiy)

    @Query("SELECT * FROM StudentEntitiy")
    fun getAllStudet(): List<Student>

}