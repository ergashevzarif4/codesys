package com.example.zarif.codesys.moduls;


public class Login {

    private String loginId;
    private String userName;
    private String userPass;
    private String passportNomer;
    private String guvohnomaNomer;
    private Boolean onStudent;
    private String userID;

    public Login() {
    }

    public Login(String loginId, String userName, String userPass, String passportNomer, String guvohnomaNomer, Boolean onStudent, String userID) {
        this.loginId = loginId;
        this.userName = userName;
        this.userPass = userPass;
        this.passportNomer = passportNomer;
        this.guvohnomaNomer = guvohnomaNomer;
        this.onStudent = onStudent;
        this.userID = userID;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public void setPassportNomer(String passportNomer) {
        this.passportNomer = passportNomer;
    }

    public void setGuvohnomaNomer(String guvohnomaNomer) {
        this.guvohnomaNomer = guvohnomaNomer;
    }

    public void setOnStudent(Boolean onStudent) {
        this.onStudent = onStudent;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Boolean getOnStudent() {
        return onStudent;
    }

    public String getLoginId() {
        return loginId;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public String getPassportNomer() {
        return passportNomer;
    }

    public String getGuvohnomaNomer() {
        return guvohnomaNomer;
    }


    public String getUserID() {
        return userID;
    }
}
