package com.example.zarif.codesys.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.example.zarif.codesys.App
import com.example.zarif.codesys.R
import com.example.zarif.codesys.configs.MY_TAG
import com.example.zarif.codesys.configs.USER_ID
import com.example.zarif.codesys.configs.showSnackBar
import com.example.zarif.codesys.listeners.IButtomAnimationHelper
import com.example.zarif.codesys.listeners.IFragmentChangeHelper
import com.example.zarif.codesys.listeners.IResponseHelper
import com.example.zarif.codesys.moduls.reponceModuls.LoginResponce
import com.example.zarif.codesys.network.menagers.StudentService
import com.example.zarif.codesys.network.menagers.TeacherService
import com.example.zarif.codesys.ui.fragments.ViewPagerFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), IFragmentChangeHelper {
    override fun showSnackBar(message: String) {
        container.showSnackBar(message)
    }

    override fun startSecondActivity(responce: LoginResponce, iButtomAnimationHelper: IButtomAnimationHelper) {
        //Toast.makeText(this, responce.toString(), Toast.LENGTH_LONG).show()
        USER_ID = responce.userId!!
        Log.d(MY_TAG, USER_ID)
        val pref = (application as App).getSharedPref()
        pref.saveIsStudentOrTeacher(responce.isStudent)
        if (responce.isStudent) {
            val studeServece = StudentService()
            studeServece.dowloandStudentAllInfo(applicationContext, USER_ID, object : IResponseHelper {
                override fun onErorr() {
                    iButtomAnimationHelper.onStop()
                }

                override fun onSucces() {
                    iButtomAnimationHelper.onStop()
                    savePref()
                    startActivity(Intent(this@MainActivity, StudentActivity::class.java))
                    finish()
                }

            })
        } else {
            val teacherService = TeacherService()
            teacherService.dowloandTeacherAllInfo(applicationContext, USER_ID, object : IResponseHelper {
                override fun onErorr() {
                    iButtomAnimationHelper.onStop()
                }

                override fun onSucces() {
                    iButtomAnimationHelper.onStop()
                    savePref()
                    startActivity(Intent(this@MainActivity, TeacherActivtiy::class.java))
                    finish()
                }
            })

        }


    }

    private fun savePref() {
        val pref = (application as App).getSharedPref()
        pref.saveFirstSeen(false)

    }

    override fun fragmentChange(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)

                .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, ViewPagerFragment())

                    .commit()
        }
    }
}
