package com.example.zarif.codesys.db.teacher;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity()
public class TeacherEntity {
    @PrimaryKey(autoGenerate = true)
    public Long entity_teacher_id;
    public String teacherId;
    public String name;
    public String lastName;
    public String kafedraName;
    public String timeTableId;
    public String imageUrl;
}

