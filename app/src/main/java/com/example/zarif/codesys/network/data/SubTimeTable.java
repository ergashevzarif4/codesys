package com.example.zarif.codesys.network.data;

public class SubTimeTable {
    private String dayOfWeek;
    private String time;
    private String room;
    private String subjectName;
    private String teacherId;

    public SubTimeTable(String dayOfWeek, String time, String room, String subjectName, String teacherId) {
        this.dayOfWeek = dayOfWeek;
        this.time = time;
        this.room = room;
        this.subjectName = subjectName;
        this.teacherId = teacherId;
    }


    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "SubTimeTable{" +
                ", dayOfWeek='" + dayOfWeek + '\'' +
                ", time='" + time + '\'' +
                ", room='" + room + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", teacherId='" + teacherId + '\'' +
                '}';
    }
}