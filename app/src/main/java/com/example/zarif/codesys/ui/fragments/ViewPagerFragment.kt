package com.example.zarif.codesys.ui.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zarif.codesys.R
import com.example.zarif.codesys.adapters.ImageViewPagerAdapter
import com.example.zarif.codesys.listeners.IFragmentChangeHelper
import com.example.zarif.codesys.listeners.IViewPagerHelper
import kotlinx.android.synthetic.main.view_pager_layout.*
import kotlinx.android.synthetic.main.view_pager_layout.view.*


class ViewPagerFragment : Fragment() {
    private var listener: IFragmentChangeHelper? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.listener = context as IFragmentChangeHelper
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = listOf("", "")
        view.view_pager.adapter = ImageViewPagerAdapter(fragmentManager, list, object : IViewPagerHelper {
            override fun changePage(i: Int) {
                view_pager.currentItem = i;
            }
        })
        view.view_pager.currentItem = 1
    }

}