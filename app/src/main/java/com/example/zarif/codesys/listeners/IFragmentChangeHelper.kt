package com.example.zarif.codesys.listeners

import android.support.v4.app.Fragment
import com.example.zarif.codesys.moduls.reponceModuls.LoginResponce

interface IFragmentChangeHelper {
    fun fragmentChange(fragment: Fragment)
    fun startSecondActivity(responce: LoginResponce, iButtomAnimationHelper: IButtomAnimationHelper);
    fun showSnackBar(message: String)
}