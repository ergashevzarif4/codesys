package com.example.zarif.codesys.ui.fragments.reg

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zarif.codesys.R
import com.example.zarif.codesys.configs.checkNetState
import com.example.zarif.codesys.configs.showError
import com.example.zarif.codesys.listeners.IFragmentChangeHelper
import com.example.zarif.codesys.listeners.IRegResponceHelper
import com.example.zarif.codesys.listeners.IViewPagerHelper
import com.example.zarif.codesys.moduls.reponceModuls.LoginResponce
import com.example.zarif.codesys.network.menagers.RegistrationServece
import com.example.zarif.codesys.ui.fragments.regi.RegistrationFragment
import kotlinx.android.synthetic.main.reg_layout.*
import kotlinx.android.synthetic.main.reg_layout.view.*

import zarifergashev.com.demo.requestModuls.RegistrationRequest

class RegFragment : Fragment() {
    private var viewPagerHelper: IViewPagerHelper? = null

    companion object {
        @JvmStatic
        fun newInstatce(helper: IViewPagerHelper) = RegFragment().apply {
            this.viewPagerHelper = helper
        }
    }

    private var listener: IFragmentChangeHelper? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as IFragmentChangeHelper
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.reg_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListener(view)

    }

    private fun setOnClickListener(view: View) {
        btn_reg.setOnClickListener {
            if (!et_guvohnoma.text.toString().isEmpty() && et_passport.text!!.isNotEmpty()) {
                val regRequest = RegistrationRequest(et_passport.text.toString(), et_guvohnoma.text.toString())
                if (view.context.checkNetState())
                    sendInfo(regRequest)
                else
                    view.showError("Couldn`t connect to the internet", View.OnClickListener {
                        startActivity(Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS))

                    })
            }
        }
        view.btn_to_Login.setOnClickListener {
            viewPagerHelper?.changePage(1)
        }
    }

    private fun sendInfo(regRequest: RegistrationRequest) {

        RegistrationServece().getRegistrationServece(regRequest, object : IRegResponceHelper {
            override fun regResponeHelper(registrationResponce: LoginResponce) {
                if (registrationResponce.message == "success") {
                    if (listener != null)
                        listener?.fragmentChange(RegistrationFragment.newInstance(registrationResponce.userId!!, registrationResponce.isStudent))

                } else
                    listener?.showSnackBar(registrationResponce.message!!)

            }
        })
    }
}