package com.example.zarif.codesys.moduls;

import com.example.zarif.codesys.db.timeTable.SubTimeTable;

import java.util.ArrayList;
import java.util.List;

public class TimeTable {
    public String timeTableId;
    public List<SubTimeTable> subTimeTables;

    public TimeTable() {
        subTimeTables = new ArrayList<>();

    }

    public TimeTable(String timeTableId, List<SubTimeTable> subTimeTables) {
        this.timeTableId = timeTableId;
        this.subTimeTables = subTimeTables;
    }

    public String getTimeTableId() {
        return timeTableId;
    }

    public void setTimeTableId(String timeTableId) {
        this.timeTableId = timeTableId;
    }

    public List<SubTimeTable> getSubTimeTables() {
        return subTimeTables;
    }

    public void setSubTimeTables(List<SubTimeTable> subTimeTables) {
        this.subTimeTables = subTimeTables;
    }

    @Override
    public String toString() {
        return "TimeTable{" +
                "timeTableId='" + timeTableId + '\'' +
                ", subTimeTables=" + subTimeTables +
                '}';
    }
}
