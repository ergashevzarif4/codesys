package com.example.zarif.codesys.ui.activity

import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import com.example.zarif.codesys.App
import com.example.zarif.codesys.db.group.GroupDao
import com.example.zarif.codesys.db.login.LoginDao
import com.example.zarif.codesys.db.student.StudentDao
import com.example.zarif.codesys.db.teacher.TeacherDao
import com.example.zarif.codesys.pref.IPreference
import com.example.zarif.codesys.pref.PreferenceIml

open class BaseActivity : AppCompatActivity() {
    //todo nima
    lateinit var app: App


    lateinit var preferences: IPreference
    lateinit var groupDao: GroupDao
    lateinit var loginDao: LoginDao
    lateinit var studentDao: StudentDao
    lateinit var teacherDao: TeacherDao
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app = this.application as App
        groupDao = app.getDatabaseManager().groupDao()
        loginDao = app.getDatabaseManager().loginDao()
        studentDao = app.getDatabaseManager().studentDao()
        teacherDao = app.getDatabaseManager().teacherDao()
        preferences = PreferenceIml(PreferenceManager.getDefaultSharedPreferences(this))
    }
}