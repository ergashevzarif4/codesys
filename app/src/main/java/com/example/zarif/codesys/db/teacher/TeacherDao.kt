package com.example.zarif.codesys.db.teacher

import android.arch.persistence.room.*

@Dao()
interface TeacherDao {
    @Insert
    fun addTeacher(teacher: TeacherEntity)

    @Delete
    fun deleteTeacher(teacher: TeacherEntity)

    @Update
    fun updateTeacher(teacher: TeacherEntity)


    @Query("select * from TeacherEntity")
    fun loadTeacher():TeacherEntity


}