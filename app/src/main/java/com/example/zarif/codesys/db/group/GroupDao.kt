package com.example.zarif.codesys.db.group

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Update

@Dao()
interface GroupDao {
    @Insert
    fun addGroup(group: GroupEntity)

    @Delete
    fun deleteGroup(group: GroupEntity)

    @Update
    fun updateGroup(group: GroupEntity)


}