package com.example.zarif.codesys.moduls;


public class Student {
    public String studentId;
    public String name;
    public String lastName;
    public String fakultetName;
    public String groupId;
    public String imageUrl;

    public Student() {
    }

    public Student(String studentId, String name, String lastName, String fakultetName, String groupId, String imageUrl) {
        this.studentId = studentId;
        this.name = name;
        this.lastName = lastName;
        this.fakultetName = fakultetName;
        this.groupId = groupId;
        this.imageUrl = imageUrl;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFakultetName() {
        return fakultetName;
    }

    public void setFakultetName(String fakultetName) {
        this.fakultetName = fakultetName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;


    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId='" + studentId + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", fakultetName='" + fakultetName + '\'' +
                ", groupId='" + groupId + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
