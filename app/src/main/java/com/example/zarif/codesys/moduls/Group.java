package com.example.zarif.codesys.moduls;


import java.util.ArrayList;
import java.util.List;


public class Group {
    public String groupId;
    public String groupName;
    public String groupKafedraName;
    public String groupTimeTableID;
    public List<String> students;

    public Group() {
        students = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupId='" + groupId + '\'' +
                ", groupName='" + groupName + '\'' +
                ", groupKafedraName='" + groupKafedraName + '\'' +
                ", groupTimeTableID='" + groupTimeTableID + '\'' +
                ", students=" + students +
                '}';
    }

    public Group(String groupId, String groupName, String groupKafedraName, String groupTimeTableID, List<String> students) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupKafedraName = groupKafedraName;
        this.groupTimeTableID = groupTimeTableID;
        this.students = students;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupKafedraName() {
        return groupKafedraName;
    }

    public void setGroupKafedraName(String groupKafedraName) {
        this.groupKafedraName = groupKafedraName;
    }

    public String getGroupTimeTableID() {
        return groupTimeTableID;
    }

    public void setGroupTimeTableID(String groupTimeTableID) {
        this.groupTimeTableID = groupTimeTableID;
    }

    public List<String> getStudents() {
        return students;
    }

    public void setStudents(List<String> students) {
        this.students = students;
    }
}
