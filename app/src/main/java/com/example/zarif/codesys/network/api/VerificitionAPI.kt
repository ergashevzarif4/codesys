package com.example.zarif.codesys.network.api

import com.example.zarif.codesys.moduls.Verifiction
import com.example.zarif.codesys.moduls.VerifictionForAdd
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface VerificitionAPI {
    @POST("/ver/add")
    fun addVerificition(@Body body: VerifictionForAdd): Call<ResponseBody>

    @GET("/ver/teacher/{id}/{date}/{time}")
    fun getByTeacherIdAndDate(@Path("id") id: String, @Path("date") date: Long, @Path("time") time: String): Call<List<Verifiction>>

    @GET("/var/teacher/{id}")
    fun getByTeacherId(@Path("id") id: String): Call<List<Verifiction>>
}
