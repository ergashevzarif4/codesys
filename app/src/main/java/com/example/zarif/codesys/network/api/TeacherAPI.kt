package com.example.zarif.codesys.network.api

import com.example.zarif.codesys.moduls.Teacher
import com.example.zarif.codesys.moduls.TimeTable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface TeacherAPI {
    @GET("/teacher/{id}")
    fun getTeacher(@Path("id") query: String): Call<Teacher>


    @GET("/studentTimeTable/{id}")
    fun getTimeTable(@Path("id") query: String): Call<TimeTable>

}