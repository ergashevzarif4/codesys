package com.example.zarif.codesys.network.menagers

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.zarif.codesys.App
import com.example.zarif.codesys.configs.BASE_URL
import com.example.zarif.codesys.configs.MY_TAG
import com.example.zarif.codesys.configs.checkNetState
import com.example.zarif.codesys.db.DBManager
import com.example.zarif.codesys.db.group.GroupEntity
import com.example.zarif.codesys.db.student.StudentEntitiy
import com.example.zarif.codesys.db.timeTable.TimeTableEntity
import com.example.zarif.codesys.listeners.IResponseHelper
import com.example.zarif.codesys.moduls.Group
import com.example.zarif.codesys.moduls.Student
import com.example.zarif.codesys.moduls.TimeTable
import com.example.zarif.codesys.network.api.StudentAPI
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class StudentService {
    private var studentServece: StudentAPI? = null
    private var studentRetrofit: Retrofit? = null
    private var context: Context? = null
    private var dbManager: DBManager? = null
    private var iResponseHelper: IResponseHelper? = null


    init {
        if (studentRetrofit == null)
            studentRetrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(OkHttpClient.Builder()
                            .connectTimeout(60, TimeUnit.SECONDS)
                            .readTimeout(60, TimeUnit.SECONDS)
                            .writeTimeout(60, TimeUnit.SECONDS)
                            .build())
                    .build()
        if (studentServece == null) {
            studentServece = studentRetrofit!!.create(StudentAPI::class.java)

        }
    }

    fun dowloandStudentAllInfo(context: Context, studentId: String, iResponseHelper: IResponseHelper) {
        this.context = context
        this.iResponseHelper = iResponseHelper
        dbManager = (context as App).getDatabaseManager()
        if (context.checkNetState()) {
            studentServece?.getStudent(studentId)?.enqueue(object : Callback<Student> {
                override fun onFailure(call: Call<Student>, t: Throwable) {
                    showErorrMessage("error");
                }

                override fun onResponse(call: Call<Student>, response: Response<Student>) {
                    if (response.isSuccessful) {
                        val student = response.body()
                        if (student != null) {
                            val newStudentEntitiy = StudentEntitiy()
                            newStudentEntitiy.fakultetName = student.fakultetName
                            newStudentEntitiy.groupId = student.groupId
                            newStudentEntitiy.imageUrl = student.imageUrl
                            newStudentEntitiy.lastName = student.lastName
                            newStudentEntitiy.name = student.name
                            newStudentEntitiy.studentId = student.studentId

                            Log.d(MY_TAG, "teacher dowloand" + student.toString())
                            dbManager?.studentDao()?.addStudent(newStudentEntitiy)
                            dowloandGroupInfo(student.groupId)

                        } else showErorrMessage("Teacher No Exist")


                    } else {
                        showErorrMessage(response.message())
                    }

                }

            })
        } else {
            showErorrMessage("not network")
        }


    }

    private fun dowloandGroupInfo(timeTableId: String) {
        studentServece?.getGroup(timeTableId)?.enqueue(object : Callback<Group> {
            override fun onFailure(call: Call<Group>, t: Throwable) {
                showErorrMessage("error");
            }

            override fun onResponse(call: Call<Group>, response: Response<Group>) {
                if (response.isSuccessful) {
                    val group = response.body()
                    if (group != null) {
                        val newGroup = GroupEntity()
                        newGroup.groupId = group.groupId
                        newGroup.groupKafedraName = group.groupKafedraName
                        newGroup.groupName = group.groupName
                        newGroup.groupTimeTableID = group.groupTimeTableID
                        newGroup.students = Gson().toJson(group.students)
                        Log.d(MY_TAG, "teacher dowloand" + group.toString())
                        Log.d(MY_TAG, "teacher dowloand" + group.students.toString())
                        dbManager?.groupDao()?.addGroup(newGroup)
                        dowloandTimeTable(group.groupTimeTableID)
                    } else showErorrMessage("Group No Exist")

                } else {
                    showErorrMessage(response.message())
                }

            }

        })

    }

    private fun dowloandTimeTable(timeTableId: String) {
        studentServece?.getTimeTable(timeTableId)?.enqueue(object : Callback<TimeTable> {
            override fun onFailure(call: Call<TimeTable>, t: Throwable) {
                showErorrMessage("error");
            }

            override fun onResponse(call: Call<TimeTable>, response: Response<TimeTable>) {
                if (response.isSuccessful) {
                    val timeTable = response.body()
                    if (timeTable != null) {
                        val newTimeTable = TimeTableEntity()
                        newTimeTable.timeTableId = timeTable.timeTableId
                        newTimeTable.subTimeTables = Gson().toJson(timeTable.subTimeTables)
                        Log.d(MY_TAG, "teacher dowloand" + timeTable.toString())
                        Log.d(MY_TAG, "teacher dowloand" + newTimeTable.subTimeTables)
                        dbManager?.timeTableDao()?.addtimeTable(newTimeTable)

                        iResponseHelper?.onSucces()
                    } else showErorrMessage("Time Table No Exist")

                } else {
                    showErorrMessage(response.message())
                }

            }

        })

    }

    private fun showErorrMessage(s: String) {
        Toast.makeText(context, s, Toast.LENGTH_LONG).show()
        iResponseHelper?.onErorr()
    }


}