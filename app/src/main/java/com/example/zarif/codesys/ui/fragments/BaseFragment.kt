package com.example.zarif.codesys.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.example.zarif.codesys.App
import com.example.zarif.codesys.db.group.GroupDao
import com.example.zarif.codesys.db.login.LoginDao
import com.example.zarif.codesys.db.student.StudentDao
import com.example.zarif.codesys.db.teacher.TeacherDao
import com.example.zarif.codesys.db.timeTable.TimeTableDao
import com.example.zarif.codesys.network.data.SubTimeTable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken



open class BaseFragment : Fragment() {
    lateinit var app: App
    var listType = object : TypeToken<ArrayList<SubTimeTable>>() {

    }.type
    var gson = Gson()
    lateinit var groupDao: GroupDao
    lateinit var loginDao: LoginDao
    lateinit var studentDao: StudentDao
    lateinit var teacherDao: TeacherDao
    lateinit var timeTableDao: TimeTableDao
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app = this.activity?.application as App
        groupDao = app.getDatabaseManager().groupDao()
        loginDao = app.getDatabaseManager().loginDao()
        studentDao = app.getDatabaseManager().studentDao()
        teacherDao = app.getDatabaseManager().teacherDao()
        timeTableDao = app.getDatabaseManager().timeTableDao()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}