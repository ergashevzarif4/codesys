package com.example.zarif.codesys.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.zarif.codesys.R
import com.example.zarif.codesys.ui.fragments.student.ProfileFragment
import com.example.zarif.codesys.ui.fragments.student.SQrFragment
import com.example.zarif.codesys.ui.fragments.student.STimeTableFragment
import kotlinx.android.synthetic.main.activity_second.*

class StudentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, STimeTableFragment())
                    .commit()



            navigation.setOnNavigationItemSelectedListener {
                when (it.itemId) {
                    R.id.time_table -> {
                        it.setIcon(R.drawable.ic_wallpaper_blue)
                        addIt(STimeTableFragment())
                        true
                    }
                    R.id.qr_table -> {
                        val dialog = SQrFragment()
                        startActivityForResult(Intent(StudentActivity@ this, QrCodeReaderActivity::class.java), 2000)
                        //    dialog.show(supportFragmentManager, "")
                        true
                    }
                    R.id.profile_table -> {
                        addIt(ProfileFragment())
                        true
                    }
                    else -> {
                        false
                    }
                }
            }
        }


    }

    fun addIt(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2000) {
            shoAnimation(QrCodeReaderActivity.STUDENT_IS_SUCCES)
        }
    }

    private fun shoAnimation(isSucces: Boolean) {


    }
}
