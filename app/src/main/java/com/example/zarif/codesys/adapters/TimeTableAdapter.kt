package com.example.zarif.codesys.adapters

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zarif.codesys.R
import com.example.zarif.codesys.configs.getTime
import com.example.zarif.codesys.network.data.SubTimeTable
import com.example.zarif.codesys.network.data.TimeTable
import kotlinx.android.synthetic.main.time_table_item.view.*

class TimeTableAdapter(var data: TimeTable) : RecyclerView.Adapter<TimeTableAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.time_table_item, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount() = data.subTimeTables.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.onBind(data.subTimeTables[p1])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(timeTable: SubTimeTable) {
            var subject:String
            if (timeTable.subjectName.length>10){
                subject=timeTable.subjectName.substringAfter(" ")
            }
            itemView.tvSubject.ellipsize = TextUtils.TruncateAt.MARQUEE
            itemView.tvSubject.isSelected = true
            itemView.tvSubject.setSingleLine(true)
            itemView.tvRomNumber.text = timeTable.room
            itemView.tvSubject.text = timeTable.subjectName
            itemView.tvBeginTime.text = timeTable.time.getTime(timeTable.time)
        }

    }

}