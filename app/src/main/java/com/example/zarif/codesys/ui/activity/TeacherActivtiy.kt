package com.example.zarif.codesys.ui.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.zarif.codesys.R
import com.example.zarif.codesys.configs.USER_ID
import com.example.zarif.codesys.network.menagers.TeacherService
import com.example.zarif.codesys.ui.fragments.teacher.ProfileFragment
import com.example.zarif.codesys.ui.fragments.teacher.QrFragment
import com.example.zarif.codesys.ui.fragments.teacher.TimeTableFragment
import kotlinx.android.synthetic.main.activity_second_activty.*

class TeacherActivtiy : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_second_activty)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, TimeTableFragment())
                    .commit()
        }

        navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.time_table -> {
                    addIt(TimeTableFragment())
                    true
                }
                R.id.qr_table -> {
                    val dialog = QrFragment()
                    dialog.show(supportFragmentManager, "")
                    true
                }
                R.id.profile_table -> {
                    addIt(ProfileFragment())
                    true
                }
                else -> {
                    false
                }
            }
        }
    }

    private fun addIt(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit()



    }
}
