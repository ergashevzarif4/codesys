package com.example.zarif.codesys.listeners

import com.example.zarif.codesys.moduls.Verifiction

interface IVerifacitionHelper {
    fun verifications(list: List<Verifiction>)

    fun erorr(message: String)
}