package com.example.zarif.codesys.network.menagers

import android.util.Log
import com.example.zarif.codesys.configs.BASE_URL
import com.example.zarif.codesys.listeners.IRegResponceHelper
import com.example.zarif.codesys.moduls.reponceModuls.LoginResponce
import com.example.zarif.codesys.moduls.reponceModuls.RegistrationResponce
import com.example.zarif.codesys.network.api.RegistrationAPI
import com.google.gson.Gson
import retrofit2.Call;
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import zarifergashev.com.demo.requestModuls.RegistrationRequest

class RegistrationServece() {
    private var gson: Gson? = null
    private var registrationServece: RegistrationAPI? = null
    private var registrationRetrofit: Retrofit? = null


    fun getRegistrationServece(registrationRequest: RegistrationRequest, iResponceHelper: IRegResponceHelper) {
        var registrationResponce = RegistrationResponce()
        if (registrationRetrofit == null)
            registrationRetrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        if (registrationServece == null) {
            registrationServece = registrationRetrofit!!.create(RegistrationAPI::class.java)

        }
        registrationServece?.registration(registrationRequest)?.enqueue(object : Callback<LoginResponce> {
            override fun onFailure(call: Call<LoginResponce>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<LoginResponce>?, response: Response<LoginResponce>?) {
                Log.d("myTag", response?.body().toString())

                if (response?.body() != null)
                    iResponceHelper.regResponeHelper(response?.body()!!)
            }
        })

    }

}