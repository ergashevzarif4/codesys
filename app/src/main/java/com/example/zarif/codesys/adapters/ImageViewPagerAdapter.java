package com.example.zarif.codesys.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.zarif.codesys.listeners.IViewPagerHelper;
import com.example.zarif.codesys.ui.fragments.login.LoginFragment;
import com.example.zarif.codesys.ui.fragments.reg.RegFragment;

import java.util.List;

public class ImageViewPagerAdapter extends FragmentStatePagerAdapter {
    List<String> images;
    IViewPagerHelper helper;

    public ImageViewPagerAdapter(FragmentManager fm, List<String> images, IViewPagerHelper helper) {
        super(fm);
        this.images = images;
        this.helper = helper;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 1)
            return LoginFragment.newInstatce(helper);
        else return RegFragment.newInstatce(helper);
    }

    @Override
    public int getCount() {
        return images.size();
    }
}
