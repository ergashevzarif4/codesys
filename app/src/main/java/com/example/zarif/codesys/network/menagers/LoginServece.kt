package com.example.zarif.codesys.network.menagers

import com.example.zarif.codesys.configs.BASE_URL
import com.example.zarif.codesys.listeners.ILoginResponceHelper
import com.example.zarif.codesys.listeners.IUserInfoChnageHelper
import com.example.zarif.codesys.moduls.Login
import com.example.zarif.codesys.moduls.reponceModuls.LoginResponce
import com.example.zarif.codesys.network.api.LoginAPI
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import zarifergashev.com.demo.requestModuls.LoginRequest
import java.util.concurrent.TimeUnit

class LoginServece {
    private var loginServece: LoginAPI? = null
    private var loginRetrofit: Retrofit? = null


    fun getRegistrationServece(loginRequest: LoginRequest, iResponceHelper: ILoginResponceHelper) {
        if (loginRetrofit == null)
            loginRetrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(OkHttpClient.Builder()
                            .connectTimeout(60, TimeUnit.SECONDS)
                            .readTimeout(60, TimeUnit.SECONDS)
                            .writeTimeout(60, TimeUnit.SECONDS)
                            .build())
                    .build()
        if (loginServece == null) {
            loginServece = loginRetrofit!!.create(LoginAPI::class.java)

        }

        loginServece?.login(loginRequest)?.enqueue(object : Callback<LoginResponce> {
            override fun onFailure(call: Call<LoginResponce>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<LoginResponce>?, response: Response<LoginResponce>?) {
                if (response?.body() != null) {

                    iResponceHelper.loginResponeHelper(response.body()!!)
                }

            }
        })

    }

    fun userInfoChange(loginRequest: Login, iResponceHelper: IUserInfoChnageHelper) {
        if (loginRetrofit == null)
            loginRetrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        if (loginServece == null) {
            loginServece = loginRetrofit!!.create(LoginAPI::class.java)

        }
        loginServece?.changeUserInfo(loginRequest)?.enqueue(object : Callback<LoginResponce> {
            override fun onFailure(call: Call<LoginResponce>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<LoginResponce>?, response: Response<LoginResponce>?) {
                if (response?.body() != null)
                    if (response.body()!!.message == "success") {
                        iResponceHelper.userInfoChange(response.body()!!)
                    }
            }
        })

    }
}