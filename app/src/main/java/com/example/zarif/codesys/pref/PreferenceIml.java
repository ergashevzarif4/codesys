package com.example.zarif.codesys.pref;

import android.content.SharedPreferences;

public class PreferenceIml implements IPreference {
    private static final String TAG = "PreferenceIml";

    private static final String SEEN = "seen";
    private static final String IS_STUDENT_OR_TEACHER = "seen";

    private SharedPreferences preferences;

    public PreferenceIml(SharedPreferences preferences) {
        this.preferences = preferences;
    }


    private void saveBoolean(String key, boolean valBool) {
        preferences.edit().putBoolean(key, valBool).apply();
    }

    private boolean loadBoolean(String key) {
        return preferences.getBoolean(key, true);
    }

    @Override
    public void saveFirstSeen(boolean isFirstTime) {
        saveBoolean(SEEN, isFirstTime);
    }

    @Override
    public boolean loadFirstSeen() {
        return loadBoolean(SEEN);
    }

    @Override
    public void saveIsStudentOrTeacher(boolean isStudentOrTeacher) {
        saveBoolean(IS_STUDENT_OR_TEACHER, isStudentOrTeacher);
    }

    @Override
    public boolean isStudentOrTeacher() {
        return loadBoolean(IS_STUDENT_OR_TEACHER);
    }


}

