package com.example.zarif.codesys.ui.fragments.student


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zarif.codesys.R
import com.example.zarif.codesys.adapters.TimeTableAdapter
import com.example.zarif.codesys.network.data.SubTimeTable
import com.example.zarif.codesys.network.data.TimeTable
import kotlinx.android.synthetic.main.item_table.*
import java.util.ArrayList

class STableFragment : Fragment() {
    lateinit var data: TimeTable
    var dayOfWeek = ""
    fun initData(data: TimeTable, dayOfWeek: String) {
        this.data = data
        this.dayOfWeek = dayOfWeek
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.item_table, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sortedData = ArrayList<SubTimeTable>()
        data.subTimeTables.forEach { it ->
            if (it.dayOfWeek == dayOfWeek) {
                sortedData.add(it)
            }
        }
        val adapter = TimeTableAdapter(TimeTable(sortedData))
        rvTimeTable.adapter = adapter
    }
}
