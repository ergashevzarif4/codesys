package com.example.zarif.codesys.moduls.reponceModuls;

public class RegistrationResponce {
    private Boolean onStudent;
    private String userId;
    private String message;

    public RegistrationResponce() {
    }

    public RegistrationResponce(Boolean onStudent, String userId, String message) {
        this.onStudent = onStudent;
        this.userId = userId;
        this.message = message;
    }

    public Boolean getStudent() {
        return onStudent;
    }

    public void setOnStudent(Boolean student) {
        onStudent = student;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "RegistrationResponce{" +
                "onStudent=" + onStudent +
                ", userId='" + userId + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
