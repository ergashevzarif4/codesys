package com.example.zarif.codesys.network.menagers

import android.content.Context
import android.widget.Toast
import com.example.zarif.codesys.configs.BASE_URL
import com.example.zarif.codesys.configs.checkNetState
import com.example.zarif.codesys.listeners.IVerifacitionHelper
import com.example.zarif.codesys.moduls.Verifiction
import com.example.zarif.codesys.moduls.VerifictionForAdd
import com.example.zarif.codesys.network.api.VerificitionAPI
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

//todo verification serveces
class VerificitionServese(private val context: Context) {
    private var verificitionServese: VerificitionAPI? = null
    private var verificitionRetrofit: Retrofit? = null


    init {
        if (verificitionRetrofit == null)
            verificitionRetrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(OkHttpClient.Builder()
                            .connectTimeout(60, TimeUnit.SECONDS)
                            .readTimeout(60, TimeUnit.SECONDS)
                            .writeTimeout(60, TimeUnit.SECONDS)
                            .build())
                    .build()
        if (verificitionServese == null) {
            verificitionServese = verificitionRetrofit!!.create(VerificitionAPI::class.java)

        }
    }

    fun addVerificitio(verifictionForAdd: VerifictionForAdd) {
        if (context.checkNetState()) {
            var call = verificitionServese?.addVerificition(verifictionForAdd)

            call?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                }
            })
        } else
            Toast.makeText(context, "not network", Toast.LENGTH_LONG).show()
    }

    fun getByTeacherIdAndDat(id: String, date: Long, time: String, iVerifacitionHelper: IVerifacitionHelper) {
        if (context.checkNetState())
            verificitionServese?.getByTeacherIdAndDate(id, date, time)?.equals(object : Callback<List<Verifiction>> {
                override fun onFailure(call: Call<List<Verifiction>>, t: Throwable) {
                    iVerifacitionHelper.erorr("FAILURE")

                }

                override fun onResponse(call: Call<List<Verifiction>>, response: Response<List<Verifiction>>) {
                    if (response.body() != null && response.body()?.size!! > 0)
                        iVerifacitionHelper.verifications(response.body()!!)
                }
            })
        else iVerifacitionHelper.erorr("not network")
    }

    fun getByTeacherId(id: String, iVerifacitionHelper: IVerifacitionHelper) {
        if (context.checkNetState())
            verificitionServese?.getByTeacherId(id)?.equals(object : Callback<List<Verifiction>> {
                override fun onFailure(call: Call<List<Verifiction>>, t: Throwable) {
                    iVerifacitionHelper.erorr("FAILURE")

                }

                override fun onResponse(call: Call<List<Verifiction>>, response: Response<List<Verifiction>>) {
                    if (response.body() != null && response.body()?.size!! > 0)
                        iVerifacitionHelper.verifications(response.body()!!)
                }
            })
        else iVerifacitionHelper.erorr("not network")
    }


}