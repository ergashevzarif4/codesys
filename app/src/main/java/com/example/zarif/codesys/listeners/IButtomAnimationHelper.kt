package com.example.zarif.codesys.listeners

interface IButtomAnimationHelper {
    fun onStart()
    fun onStop()
}