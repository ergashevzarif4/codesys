package com.example.zarif.codesys.ui.fragments.regi

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zarif.codesys.R
import com.example.zarif.codesys.configs.checkNetState
import com.example.zarif.codesys.configs.showError
import com.example.zarif.codesys.listeners.IButtomAnimationHelper
import com.example.zarif.codesys.listeners.IFragmentChangeHelper
import com.example.zarif.codesys.listeners.IUserInfoChnageHelper
import com.example.zarif.codesys.moduls.Login
import com.example.zarif.codesys.moduls.reponceModuls.LoginResponce
import com.example.zarif.codesys.network.menagers.LoginServece
import kotlinx.android.synthetic.main.registration_fragment.*
import kotlinx.android.synthetic.main.registration_fragment.view.*

class RegistrationFragment : Fragment() {
    private var userId: String? = null
    private var isStudent: Boolean? = null
    private var listeen: IFragmentChangeHelper? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listeen = context as IFragmentChangeHelper
    }

    companion object {
        @JvmStatic
        fun newInstance(userId: String, student: Boolean) =
                RegistrationFragment().apply {
                    arguments = Bundle().apply {
                        putString("userId", userId)
                        putBoolean("isStudent", student)
                    }
                }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userId = arguments?.getString("userId")
        isStudent = arguments?.getBoolean("isStudent")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.reg_fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.btn_login.setOnClickListener {
            if (!view.et_login.text.toString().isNullOrEmpty()) {
                if (!view.et_password.text.toString().isNullOrEmpty() && !view.et_password_2.text.toString().isNullOrEmpty())
                    if (view.et_password.text.toString() == view.et_password_2.text.toString()) {
                        val login = Login()
                        login.loginId = userId
                        login.userName = view.et_login.text.toString()
                        login.userPass = view.et_password.text.toString()
                        if (view.context.checkNetState()) {
                            startButtonAnimation()
                            LoginServece().userInfoChange(login, object : IUserInfoChnageHelper {
                                override fun userInfoChange(responce: LoginResponce) {
                                    listeen?.startSecondActivity(responce, object : IButtomAnimationHelper {
                                        override fun onStart() {
                                            startButtonAnimation()
                                        }
                                        override fun onStop() {
                                            stopButtonAnimation()
                                        }
                                    })
                                }
                            })
                        } else
                            view.showError("Couldn`t connect to the internet", View.OnClickListener {
                                startActivity(Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS))
                            })
                    } else
                        listeen?.showSnackBar("passwords in correct")
                else
                    listeen?.showSnackBar("password is empty")
            } else {
                listeen?.showSnackBar("login is empty")
            }

        }
    }

    private fun stopButtonAnimation() {
        btn_login.startAnimation()

    }

    private fun startButtonAnimation() {
        btn_login.startAnimation()
    }

}