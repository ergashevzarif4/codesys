package com.example.zarif.codesys.network.api

import com.example.zarif.codesys.moduls.reponceModuls.LoginResponce
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import zarifergashev.com.demo.requestModuls.RegistrationRequest


interface RegistrationAPI {
    //@FormUrlEncoded
    @POST("reg")
    fun registration(@Body request: RegistrationRequest): Call<LoginResponce>

}