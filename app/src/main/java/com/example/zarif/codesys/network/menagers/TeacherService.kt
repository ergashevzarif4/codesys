package com.example.zarif.codesys.network.menagers

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.zarif.codesys.App
import com.example.zarif.codesys.configs.BASE_URL
import com.example.zarif.codesys.configs.MY_TAG
import com.example.zarif.codesys.configs.checkNetState
import com.example.zarif.codesys.db.DBManager
import com.example.zarif.codesys.db.teacher.TeacherEntity
import com.example.zarif.codesys.db.timeTable.TimeTableEntity
import com.example.zarif.codesys.listeners.IResponseHelper
import com.example.zarif.codesys.moduls.Teacher
import com.example.zarif.codesys.moduls.TimeTable
import com.example.zarif.codesys.network.api.TeacherAPI
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class TeacherService {
    private var teacherService: TeacherAPI? = null
    private var teacherRetrofit: Retrofit? = null
    private var context: Context? = null
    private var dbManager: DBManager? = null
    private var iResponseHelper: IResponseHelper? = null

    init {
        if (teacherRetrofit == null)
            teacherRetrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(OkHttpClient.Builder()
                            .connectTimeout(60, TimeUnit.SECONDS)
                            .readTimeout(60, TimeUnit.SECONDS)
                            .writeTimeout(60, TimeUnit.SECONDS)
                            .build())
                    .build()
        if (teacherService == null) {
            teacherService = teacherRetrofit!!.create(TeacherAPI::class.java)

        }
    }

    fun dowloandTeacherAllInfo(context: Context, teacherId: String, iResponseHelper: IResponseHelper) {
        this.context = context
        this.iResponseHelper = iResponseHelper
        dbManager = (context as App).getDatabaseManager()
        if (context.checkNetState()) {
            teacherService?.getTeacher(teacherId)?.enqueue(object : Callback<Teacher> {
                override fun onFailure(call: Call<Teacher>, t: Throwable) {
                    showErorrMessage("error");
                    iResponseHelper.onErorr()
                }

                override fun onResponse(call: Call<Teacher>, response: Response<Teacher>) {
                    if (response.isSuccessful) {
                        val teacher = response.body()
                        if (teacher != null) {

                            val newTeacherEntity = TeacherEntity()
                            newTeacherEntity.imageUrl = teacher.imageUrl
                            newTeacherEntity.kafedraName = teacher.kafedraName
                            newTeacherEntity.lastName = teacher.lastName
                            newTeacherEntity.name = teacher.name
                            newTeacherEntity.teacherId = teacher.teacherId
                            newTeacherEntity.timeTableId = teacher.timeTableId
                            Log.d(MY_TAG, "teacher dowloand" + teacher.toString())
                            dbManager?.teacherDao()?.addTeacher(newTeacherEntity)


                            dowloandTimeTable(newTeacherEntity.timeTableId)

                        } else showErorrMessage("Teacher No Exist")


                    } else {
                        showErorrMessage(response.message())
                    }

                }

            })
        } else {
            showErorrMessage("not network")
        }


    }

    private fun dowloandTimeTable(timeTableId: String) {
        teacherService?.getTimeTable(timeTableId)?.enqueue(object : Callback<TimeTable> {
            override fun onFailure(call: Call<TimeTable>, t: Throwable) {
                showErorrMessage("error");
            }

            override fun onResponse(call: Call<TimeTable>, response: Response<TimeTable>) {
                if (response.isSuccessful) {
                    val timeTable = response.body()
                    if (timeTable != null) {
                        val newTimeTable = TimeTableEntity()
                        newTimeTable.timeTableId = timeTable.timeTableId
                        newTimeTable.subTimeTables = Gson().toJson(timeTable.subTimeTables)

                        Log.d(MY_TAG, "teacher dowloand" + timeTable.toString())
                        Log.d(MY_TAG, "teacher dowloand" + newTimeTable.subTimeTables)

                        dbManager?.timeTableDao()?.addtimeTable(newTimeTable)

                        iResponseHelper?.onSucces()

                    } else showErorrMessage("Teacher No Exist")


                } else {
                    showErorrMessage(response.message())
                }

            }

        })

    }

    private fun showErorrMessage(s: String) {
        Toast.makeText(context, s, Toast.LENGTH_LONG).show()
        iResponseHelper?.onErorr()
    }

}