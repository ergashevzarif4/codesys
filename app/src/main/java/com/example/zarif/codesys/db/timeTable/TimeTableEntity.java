package com.example.zarif.codesys.db.timeTable;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class TimeTableEntity {
    @PrimaryKey(autoGenerate = true)
    public Long time_table_entity_id;
    public String timeTableId;
    public String subTimeTables;
}
