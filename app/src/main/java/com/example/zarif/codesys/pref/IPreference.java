package com.example.zarif.codesys.pref;

public interface IPreference {

    void saveFirstSeen(boolean isFirstTime);

    boolean loadFirstSeen();

    void saveIsStudentOrTeacher(boolean isStudentOrTeacher);

    boolean isStudentOrTeacher();

}
