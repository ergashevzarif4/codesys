package com.example.zarif.codesys.listeners

import com.example.zarif.codesys.network.data.SubTimeTable

interface ITeacherTimeTableHelper {
    fun generateBarCode(subTimeTable: SubTimeTable)
    fun erorr(message: String)
}