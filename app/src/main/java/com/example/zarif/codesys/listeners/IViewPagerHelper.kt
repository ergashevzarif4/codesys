package com.example.zarif.codesys.listeners

interface IViewPagerHelper {
    fun changePage(i: Int)
}
