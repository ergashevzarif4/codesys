package com.example.zarif.codesys.configs

import android.content.Context
import android.net.ConnectivityManager
import android.support.design.widget.Snackbar
import android.view.View
import com.example.zarif.codesys.enum.*
import com.example.zarif.codesys.moduls.Verifiction

const val BASE_URL = "http://192.168.43.156:8080/"
const val MY_TAG = "MyTag"
const val SHARED_PREF_NAME = "SHARED_PREF_NAME"
const val SHARED_PREF_VERSION = 1




fun View.showSnackBar(message: String) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    snackbar.show()
}

var USER_ID = ""


fun View.showError(messageRes: String, listener: View.OnClickListener? = null) {
    Snackbar.make(this, messageRes, Snackbar.LENGTH_LONG).apply {
        if (listener != null)
            setAction("try Again", listener)
        show()
    }
}

fun Context.checkNetState(): Boolean {
    val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return manager.activeNetworkInfo != null
}

fun String.getTime(timeToEnum: String): String {

    if (timeToEnum == BIRINCHI_PARA)
        return "08:30 \n09:50"
    else if (timeToEnum == IKKINCHI_PARA)
        return "10:00 \n11:20"
    else if (timeToEnum == UCHUNCHI_PARA)
        return "11:30 \n12:50"
    else if (timeToEnum == TURTINCHI_PARA)
        return "13:30 \n14:50"
    else if (timeToEnum == BESHINCHI_PARA)
        return "15:00 \n16:20"
    else if (timeToEnum == OLTINCHI_PARA)
        return "16:30 \n18:50"
    else return ""
}

