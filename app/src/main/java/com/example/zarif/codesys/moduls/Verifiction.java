package com.example.zarif.codesys.moduls;


public class Verifiction {

    private String verifictionId;
    private String studentId;
    private String teacherId;
    private String groupId;
    private String checkInTime;
    private String checkOutTime;
    private String time;
    private Long date;

    public Verifiction() {
    }

    public Verifiction(String verifictionId, String studentId, String teacherId, String groupId, String checkInTime, String checkOutTime, String time, Long date) {
        this.verifictionId = verifictionId;
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.groupId = groupId;
        this.checkInTime = checkInTime;
        this.checkOutTime = checkOutTime;
        this.time = time;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Verifiction{" +
                "verifictionId='" + verifictionId + '\'' +
                ", studentId='" + studentId + '\'' +
                ", teacherId='" + teacherId + '\'' +
                ", groupId='" + groupId + '\'' +
                ", checkInTime='" + checkInTime + '\'' +
                ", checkOutTime='" + checkOutTime + '\'' +
                ", time='" + time + '\'' +
                ", date=" + date +
                '}';
    }

    public String getVerifictionId() {
        return verifictionId;
    }

    public void setVerifictionId(String verifictionId) {
        this.verifictionId = verifictionId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
