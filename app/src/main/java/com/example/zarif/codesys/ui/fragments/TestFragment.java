/*
package com.example.zarif.codesys.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mruzbek.timetable.R;
import com.example.mruzbek.timetable.timeTable.mainActivity.DayTimeTable;
import com.example.mruzbek.timetable.timeTable.mainActivity.MainActivity;

import java.util.Calendar;


public class TestFragment extends Fragment {
    private static final String TAG = "TestFragment";
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.app_bar_main, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());


        mViewPager = view.findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        TabLayout tabLayout = view.findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        avtoDeterminationDay();
    }

    private void avtoDeterminationDay() {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        switch (day) {

            case Calendar.MONDAY:
                mViewPager.setCurrentItem(0);
                break;
            case Calendar.TUESDAY:
                mViewPager.setCurrentItem(1);
                break;
            case Calendar.WEDNESDAY:
                mViewPager.setCurrentItem(2);
                break;
            case Calendar.THURSDAY:
                mViewPager.setCurrentItem(3);
                break;
            case Calendar.FRIDAY:
                mViewPager.setCurrentItem(4);
                break;
            case Calendar.SATURDAY:
                mViewPager.setCurrentItem(5);
                break;
            default:
                mViewPager.setCurrentItem(0);
                break;
        }
    }

    class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public void startUpdate(ViewGroup container) {

            super.startUpdate(container);
            Log.d(TAG, "startUpdate: ");
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt(MainActivity.POSITION, position);
            DayTimeTable table = new DayTimeTable();
            table.setArguments(bundle);

            return table;
        }

        @Override
        public int getCount(
        ) {
            return 6;
        }
    }
}

*/
