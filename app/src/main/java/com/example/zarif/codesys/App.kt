package com.example.zarif.codesys

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.example.zarif.codesys.configs.SHARED_PREF_NAME
import com.example.zarif.codesys.db.DBManager
import com.example.zarif.codesys.pref.PreferenceIml


class App : Application() {
    private lateinit var databaseManager: DBManager
    private lateinit var pref: PreferenceIml


    override fun onCreate() {
        super.onCreate()
        databaseManager = Room.databaseBuilder(this, DBManager::class.java, "mydb")
                .allowMainThreadQueries()
                .build()
        pref = PreferenceIml(applicationContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE))

    }

    fun getDatabaseManager() = databaseManager

    fun getSharedPref() = pref
}