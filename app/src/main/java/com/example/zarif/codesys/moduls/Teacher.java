package com.example.zarif.codesys.moduls;

public class Teacher {
    public String teacherId;
    public String name;
    public String lastName;
    public String kafedraName;
    public String timeTableId;
    public String imageUrl;

    public Teacher() {
    }

    public Teacher(String teacherId, String name, String lastName, String kafedraName, String timeTableId, String imageUrl) {
        this.teacherId = teacherId;
        this.name = name;
        this.lastName = lastName;
        this.kafedraName = kafedraName;
        this.timeTableId = timeTableId;
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teacherId='" + teacherId + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", kafedraName='" + kafedraName + '\'' +
                ", timeTableId='" + timeTableId + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getKafedraName() {
        return kafedraName;
    }

    public void setKafedraName(String kafedraName) {
        this.kafedraName = kafedraName;
    }

    public String getTimeTableId() {
        return timeTableId;
    }

    public void setTimeTableId(String timeTableId) {
        this.timeTableId = timeTableId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

