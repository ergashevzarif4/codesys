package com.example.zarif.codesys.db.student;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class StudentEntitiy {
    @PrimaryKey(autoGenerate = true)
    public Long student_entity_id;
    public String studentId;
    public String name;
    public String lastName;
    public String fakultetName;
    public String groupId;
    public String imageUrl;

}
