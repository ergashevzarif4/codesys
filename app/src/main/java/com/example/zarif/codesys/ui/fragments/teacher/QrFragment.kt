package com.example.zarif.codesys.ui.fragments.teacher


import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zarif.codesys.App
import com.example.zarif.codesys.R
import com.example.zarif.codesys.configs.ForTEacher
import com.example.zarif.codesys.listeners.ITeacherTimeTableHelper
import com.example.zarif.codesys.network.data.SubTimeTable
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.fragment_qr.*

class QrFragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_qr, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var key = ""
        val manager = (this.activity?.application as App).getDatabaseManager()
        ForTEacher(manager).checkTeacher(object : ITeacherTimeTableHelper {
            override fun generateBarCode(subTimeTable: SubTimeTable) {
                ivQrCode.visibility = View.VISIBLE
                ivLottie.visibility = View.GONE
                key = Gson().toJson(subTimeTable)
                val writer = MultiFormatWriter()
                val bitMatrix = writer.encode(key, BarcodeFormat.QR_CODE, 900, 900)
                val encoder = BarcodeEncoder()
                val bitmap = encoder.createBitmap(bitMatrix)
                ivQrCode.setImageBitmap(bitmap)
            }

            override fun erorr(message: String) {
                ivQrCode.visibility = View.GONE
                ivLottie.visibility = View.VISIBLE
            }
        })

        btnClose.setOnClickListener { dialog.dismiss() }
    }

    /*override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.window.setGravity(Gravity.BOTTOM)
        }
    }
*/
}
