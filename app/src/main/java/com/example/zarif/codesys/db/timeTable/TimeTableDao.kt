package com.example.zarif.codesys.db.timeTable

import android.arch.persistence.room.*

@Dao
interface TimeTableDao {
    @Insert
    fun addtimeTable(timeTable: TimeTableEntity)

    @Delete
    fun deletetimeTable(timeTable: TimeTableEntity)

    @Update
    fun updatetimeTable(timeTable: TimeTableEntity)

    @Query("select * from TimeTableEntity")
    fun loadTimeTable(): TimeTableEntity

}