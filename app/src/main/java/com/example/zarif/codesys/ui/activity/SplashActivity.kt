package com.example.zarif.codesys.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.example.zarif.codesys.App
import com.example.zarif.codesys.R

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val pref = (application as App).getSharedPref()
        if (pref.loadFirstSeen()) {
            Handler().postDelayed({
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }, 3000)
        } else {
            Handler().postDelayed({
                if (pref.isStudentOrTeacher)
                    startActivity(Intent(this, StudentActivity::class.java))
                else
                    startActivity(Intent(this, TeacherActivtiy::class.java))
            }, 1500)
        }
    }
}
