package com.example.zarif.codesys.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.zarif.codesys.db.group.GroupDao
import com.example.zarif.codesys.db.group.GroupEntity
import com.example.zarif.codesys.db.login.LoginDao
import com.example.zarif.codesys.db.login.LoginEntity
import com.example.zarif.codesys.db.student.StudentDao
import com.example.zarif.codesys.db.student.StudentEntitiy
import com.example.zarif.codesys.db.teacher.TeacherDao
import com.example.zarif.codesys.db.teacher.TeacherEntity
import com.example.zarif.codesys.db.timeTable.TimeTableDao
import com.example.zarif.codesys.db.timeTable.TimeTableEntity

@Database(entities = [GroupEntity::class, LoginEntity::class, StudentEntitiy::class, TeacherEntity::class, TimeTableEntity::class], version = 1)
abstract class DBManager : RoomDatabase() {
    abstract fun groupDao(): GroupDao
    abstract fun loginDao(): LoginDao
    abstract fun studentDao(): StudentDao
    abstract fun teacherDao(): TeacherDao
    abstract fun timeTableDao(): TimeTableDao

}