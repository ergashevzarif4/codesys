package com.example.zarif.codesys.ui.fragments.teacher


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zarif.codesys.R
import com.example.zarif.codesys.network.data.SubTimeTable
import com.example.zarif.codesys.network.data.TimeTable
import com.example.zarif.codesys.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_time_table.*

class TimeTableFragment : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_time_table, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTableFragment("mon",true)
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                when (p0?.text) {
                    "Mon" -> {
                        setTableFragment("mon")
                    }
                    "Tue" -> {
                        setTableFragment("tue")
                    }
                    "Wed" -> {
                        setTableFragment("wed")
                    }
                    "Thu" -> {
                        setTableFragment("thu")
                    }
                    "Fri" -> {
                        setTableFragment("fri")
                    }
                    "Sat" -> {
                        setTableFragment("sat")
                    }
                    else -> {
                    }
                }
            }

        })
        setTableFragment("mon", true)
    }

    private fun setTableFragment(dayOfWeek: String, isFirst: Boolean? = false) {
        val json = timeTableDao.loadTimeTable().subTimeTables
        val data: ArrayList<SubTimeTable> = gson.fromJson(json, listType)
        val fragment = TTableFragment()
        val item = TimeTable(data)
        fragment.initData(item, dayOfWeek)
        if (isFirst!!) {
            fragmentManager?.beginTransaction()
                    ?.add(R.id.inContainer, fragment)
                    ?.commit()
        } else {
            fragmentManager?.beginTransaction()
                    ?.replace(R.id.inContainer, fragment)
                    ?.commit()
        }


    }

}
