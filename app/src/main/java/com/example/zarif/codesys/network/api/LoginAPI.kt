package com.example.zarif.codesys.network.api

import com.example.zarif.codesys.moduls.Login
import com.example.zarif.codesys.moduls.reponceModuls.LoginResponce
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import zarifergashev.com.demo.requestModuls.LoginRequest

interface LoginAPI {

    // @FormUrlEncoded
    @POST("login")
    fun login(@Body request: LoginRequest): Call<LoginResponce>


    //@FormUrlEncoded
    @POST("login/change")
    fun changeUserInfo(@Body request: Login): Call<LoginResponce>
}