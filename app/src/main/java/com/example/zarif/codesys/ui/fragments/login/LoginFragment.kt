package com.example.zarif.codesys.ui.fragments.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zarif.codesys.R
import com.example.zarif.codesys.configs.MY_TAG
import com.example.zarif.codesys.configs.checkNetState
import com.example.zarif.codesys.configs.showError
import com.example.zarif.codesys.listeners.IButtomAnimationHelper
import com.example.zarif.codesys.listeners.IFragmentChangeHelper
import com.example.zarif.codesys.listeners.ILoginResponceHelper
import com.example.zarif.codesys.listeners.IViewPagerHelper
import com.example.zarif.codesys.moduls.reponceModuls.LoginResponce
import com.example.zarif.codesys.network.menagers.LoginServece
import kotlinx.android.synthetic.main.signin_layout.*
import kotlinx.android.synthetic.main.signin_layout.view.*
import zarifergashev.com.demo.requestModuls.LoginRequest

class LoginFragment : Fragment() {
    private var listener: IFragmentChangeHelper? = null
    private var viewPagerHelper: IViewPagerHelper? = null

    companion object {
        @JvmStatic
        fun newInstatce(helper: IViewPagerHelper) = LoginFragment().apply {
            this.viewPagerHelper = helper
        }
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.listener = context as IFragmentChangeHelper
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.signin_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_login.setOnClickListener {
            if (!et_login.text.toString().isNullOrEmpty() && !et_password.text.toString().isNullOrEmpty()) {
                val request = LoginRequest(et_login.text.toString(), et_password.text.toString())
                if (view.context.checkNetState()) {
                    startButtonAnimation()
                    LoginServece().getRegistrationServece(request, object : ILoginResponceHelper {
                        override fun loginResponeHelper(loginResponce: LoginResponce) {
                            if (loginResponce.message == "success") {
                                var re = LoginResponce()
                                re.isStudent = (loginResponce.isStudent)
                                re.userId = loginResponce.userId
                                re.message = re.message
                                Log.d(MY_TAG, loginResponce.toString())
                                listener?.startSecondActivity(re, object : IButtomAnimationHelper {
                                    override fun onStart() {
                                        startButtonAnimation()

                                    }

                                    override fun onStop() {
                                        stopButtonAnimation()
                                    }
                                })
                            } else {
                                stopButtonAnimation()
                                listener?.showSnackBar(loginResponce.message ?: "")
                            }
                        }
                    })
                } else
                    view.showError("Couldn`t connect to the internet", View.OnClickListener {
                        startActivity(Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS))

                    })
            } else {
                listener?.showSnackBar("login or password is empty")
            }
        }
        view.btn_to_Reg.setOnClickListener {
            viewPagerHelper?.changePage(0)
        }
        /*
        view.btn_far.setOnClickListener {
            listener?.fragmentChange(RegFragment())
        }
        view.btn_reg.setOnClickListener {
            listener?.fragmentChange(RegFragment())
        }*/

    }

    private fun startButtonAnimation() {
        btn_login.startAnimation()
    }

    private fun stopButtonAnimation() {
        btn_login.stopAnimation()
    }

}