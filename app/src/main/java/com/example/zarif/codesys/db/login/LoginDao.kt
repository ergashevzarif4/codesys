package com.example.zarif.codesys.db.login

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Update

@Dao
interface LoginDao {
    @Insert
    fun addLogin(login: LoginEntity)

    @Delete
    fun deleteLogin(login: LoginEntity)

    @Update
    fun updateLogin(login: LoginEntity)

}