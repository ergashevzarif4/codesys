package com.example.zarif.codesys.network.api

import com.example.zarif.codesys.moduls.Group
import com.example.zarif.codesys.moduls.Student
import com.example.zarif.codesys.moduls.TimeTable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface StudentAPI {

    @GET("/students/student/{id}")
    fun getStudent(@Path("id") query: String): Call<Student>

    @GET("/group/get/{id}")
    fun getGroup(@Path("id") query: String): Call<Group>

    @GET("/studentTimeTable/{id}")
    fun getTimeTable(@Path("id") query: String): Call<TimeTable>

}