package com.example.zarif.codesys.configs

import com.example.zarif.codesys.db.DBManager
import com.example.zarif.codesys.enum.*
import com.example.zarif.codesys.listeners.ICheckTimeTableHelper
import com.example.zarif.codesys.moduls.VerifictionAdd
import com.example.zarif.codesys.moduls.VerifictionForAdd
import com.example.zarif.codesys.network.data.SubTimeTable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*

//todo verificitionni tekshirish uchun
class ForStudents(private val dbManager: DBManager) {

    fun checkTimeTable(teacherTimeTable: SubTimeTable, iErorrHelper: ICheckTimeTableHelper) {
        val subtimeTableString = dbManager.timeTableDao().loadTimeTable().subTimeTables
        val groupId = dbManager.studentDao().getAllStudet()[0].groupId
        val studentId = dbManager.studentDao().getAllStudet()[0].studentId
        val typ: Type = object : TypeToken<List<SubTimeTable>>() {}.type
        var subTimeTable: MutableList<SubTimeTable> = Gson().fromJson(subtimeTableString, typ)
        var studentTimeTable: SubTimeTable? = null
        for (subTimeTable in subTimeTable) {
            if (checkTime(subTimeTable.time, subTimeTable.dayOfWeek)) {
                studentTimeTable = subTimeTable
                break
            }
        }
        if (studentTimeTable == null) {
            iErorrHelper.erorr("lesson no found")
        } else {
            if (teacherTimeTable.teacherId == studentTimeTable.teacherId &&
                    teacherTimeTable.dayOfWeek == studentTimeTable.dayOfWeek &&
                    teacherTimeTable.room == studentTimeTable.room &&
                    teacherTimeTable.time == studentTimeTable.time
                    && teacherTimeTable.subjectName == studentTimeTable.subjectName) {
                val verification = VerifictionForAdd()
                verification.date = Calendar.getInstance().timeInMillis
                verification.groupId = groupId
                verification.teacherId = teacherTimeTable.teacherId
                verification.studentId = studentId
                verification.time = teacherTimeTable.time
                iErorrHelper.addRequestVerification(verification)
            } else {
                iErorrHelper.erorr("bu o'qtuvchi emas boshqasi darsga kiradi")
            }
        }


    }

    private fun checkTime(time: String?, dayOfWeek: String?): Boolean {
//todo dayofWekni tekshiramiz
        val b: Boolean
        val a: Boolean
        val date = Calendar.getInstance()
        if (time == BIRINCHI_PARA && date.get(Calendar.HOUR_OF_DAY) == 8 && date.get(Calendar.MINUTE) < 35 && date.get(Calendar.MINUTE) > 20)
            b = true
        else if (time == IKKINCHI_PARA && (date.get(Calendar.HOUR_OF_DAY) == 9 && (date.get(Calendar.MINUTE) > 50) || (date.get(Calendar.HOUR_OF_DAY) == 10 && date.get(Calendar.MINUTE) < 5)))
            b = true
        else if (time == UCHUNCHI_PARA && date.get(Calendar.HOUR_OF_DAY) == 11 && date.get(Calendar.MINUTE) < 35 && date.get(Calendar.MINUTE) > 20)
            b = true
        else if (time == TURTINCHI_PARA && date.get(Calendar.HOUR_OF_DAY) == 13 && date.get(Calendar.MINUTE) < 35 && date.get(Calendar.MINUTE) > 20)
            b = true
        else if (time == BESHINCHI_PARA && (date.get(Calendar.HOUR_OF_DAY) == 14 && (date.get(Calendar.MINUTE) > 50) || (date.get(Calendar.HOUR_OF_DAY) == 15 && date.get(Calendar.MINUTE) < 5)))
            b = true
        else b = time == OLTINCHI_PARA && date.get(Calendar.HOUR_OF_DAY) == 16 && date.get(Calendar.MINUTE) < 35 && date.get(Calendar.MINUTE) > 20

        val dayofWeek = date.get(Calendar.DAY_OF_WEEK)
        if (dayofWeek == 1)
            a = dayOfWeek == "sun"
        else if (dayofWeek == 2)
            a = dayOfWeek == "mon"
        else if (dayofWeek == 3)
            a = dayOfWeek == "tue"
        else if (dayofWeek == 4)
            a = dayOfWeek == "wen"
        else if (dayofWeek == 5)
            a = dayOfWeek == "thu"
        else if (dayofWeek == 6)
            a = dayOfWeek == "fre"
        else if (dayofWeek == 7)
            a = dayOfWeek == "sat"
        else a = false

        return a && b
    }
}